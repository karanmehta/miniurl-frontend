import axios from 'axios';
import { ENDPOINT, SHORTURLENPOINT } from '../config';
import * as types from './actionTypes';

export function fetchShortUrl(url) {
    return function (dispatch) {
        console.log(url)
        let shortId = '';
        if(url !== undefined || url !== null) {
            dispatch(setLoading());
        }
        if(url && url.indexOf(SHORTURLENPOINT) !== -1) {
            let arr = url.split('/');
            shortId = arr[arr.length-1];
        }

        if(shortId) {
            const request = `${ENDPOINT}/miniurl/${shortId}`;
            return axios.get(request).then((response) => {
                return dispatch({
                    type: types.GET_URL,
                    payload: response.data.url
                });
            }).catch(function (error) {
                return dispatch({
                    type: types.ERROR,
                    payload: JSON.stringify(error)
                });
            });
        } else {
            const request = `${ENDPOINT}/miniurl`;
            return axios.post(request, {
                url: url
            }).then((response) => {
                return dispatch({
                    type: types.GET_URL,
                    payload: `${SHORTURLENPOINT}/${response.data.shortId}`
                });
            }).catch(function (error) {
                console.log(error.response)
                return dispatch({
                    type: types.ERROR,
                    payload: JSON.stringify(error)
                });
            });
        }
    };

}

export function setLoading() {
    return({
        type: types.LOADING,
        payload: true
    });
}
