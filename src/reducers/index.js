import { combineReducers } from 'redux';
import UrlReducer from './urlReducer';

const rootReducer = combineReducers({
  minifiedUrl: UrlReducer
});

export default rootReducer;
