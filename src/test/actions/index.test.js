import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';
import * as actions from './../../actions/actionCreators';
import * as actionTypes from './../../actions/actionTypes';
import { ENDPOINT, SHORTURLENPOINT } from './../../config'
const middleware = [thunk]
const mockStore = configureMockStore(middleware)

describe('Actions' , () => {
    describe('Loading', () => {
        const store = mockStore({
            minifiedUrl: {
                loading: false,
                url: null,
                error: null
            }
        });
        const res = store.dispatch(actions.setLoading())
        it('Loading payload', () => {
            expect(res).toEqual({
                type: actionTypes.LOADING,
                payload: true
            });
        });
    });

    describe('Fetch URL', () => {
        it('Check loading dispatch', () => {
            const store = mockStore({
                minifiedUrl: {
                    loading: false,
                    url: null,
                    error: null
                }
            });
            const url = 'http://facebook.com'
            store.dispatch(actions.fetchShortUrl(url))
            let res = store.getActions()
            expect(res).toEqual([{
                type: actionTypes.LOADING,
                payload: true
            }]);
        });

        it('Shorten URL dispatch', () => {
            const store = mockStore({
                minifiedUrl: {
                    loading: false,
                    url: null,
                    error: null
                }
            });
            nock(`${ENDPOINT}/miniurl`)
                .post('')
                .reply(200, {data: {shortId: '111111'}});

            const url = 'http://facebook.com'
            return store.dispatch(actions.fetchShortUrl(url)).then((response) => {
                return store.dispatch(actions.fetchShortUrl(data)).then(() => {
                    expect(store.getActions()).toEqual([{
                        type: 'LOADING',
                        payload: true 
                    }, {
                        type: 'GET_URL',
                        payload: `${SHORTURLENPOINT}/111111`
                    }]);
                });
            });
        });

        it('Retrieve URL dispatch', () => {
            const store = mockStore({
                minifiedUrl: {
                    loading: false,
                    url: null,
                    error: null
                }
            });
            nock(`${ENDPOINT}/miniurl/111111`)
                .post('')
                .reply(200, {data: {url: 'http://facebook.com'}});

            const url = `${SHORTURLENPOINT}/111111`
            return store.dispatch(actions.fetchShortUrl(url)).then((response) => {
                return store.dispatch(actions.fetchShortUrl(data)).then(() => {
                    expect(store.getActions()).toEqual([{
                        type: 'LOADING',
                        payload: true 
                    }, {
                        type: 'GET_URL',
                        payload: 'http://facebook.com'
                    }]);
                });
            });
        });
    });
});
