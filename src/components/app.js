import React, { Component } from 'react';
import * as Redux from 'react-redux';

import SearchUrl from './searchUrl';
import DisplayUrl from './displayUrl';

class App extends Component {

    render() {
        return (
            <div className="container vertical-center">
                <div className="col-xs-hidden col-md-3"></div>
                <div className="col-xs-12 col-md-8">
                    <div className="panel panel-info">
                        <div className="panel-heading">
                            <h4>mini-url</h4>
                        </div>
                        <div className="panel-body">
                            <SearchUrl error={this.props.error} />
                            <DisplayUrl result={this.props.url} loading={this.props.loading} />
                        </div>
                    </div>
                </div>
                <div className="col-xs-hidden col-md-3"></div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        ...state,
        url: state.minifiedUrl.url,
        loading: state.minifiedUrl.loading,
        error: state.minifiedUrl.error
    }
}
export default Redux.connect(mapStateToProps)(App);
